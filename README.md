# GraphViz

## [Webgraphviz](http://www.webgraphviz.com/)

## [Viz](http://viz-js.com/)

[Repository on GitHub](https://github.com/mdaines/viz.js)

## [Project Management as Code with Graphviz](https://zwischenzugs.com/2017/12/18/project-management-as-code-with-graphviz/) by Zwischenzugs

## WSL

```wsl
sudo apt install graphviz -y
dot -Tpng graph.dot -ograph.png
```

https://en.wikipedia.org/wiki/DOT_(graph_description_language)

```dot
graph graph {
    a -- b -- c;
    b -- d;
}
```

- `graph` - no arrows, just `--`
- `digraph` - arrows `->` and `<-`

## VS Code

https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz
